#!/bin/bash

buildRoot=/build
installDstRoot=/usr/local

srcRoot=$buildRoot/src
buildTmpRoot=$buildRoot/tmp
pkgRoot=$buildRoot/pkg
scriptRoot=$buildRoot/script

cd $installDstRoot
cat $pkgRoot/jdk-8u111-linux-x64.tar.gz.0* | tar zxv
ln -s jdk1.8.0_111 jdk

tar zxvf $pkgRoot/apache-maven-3.5.2-bin.tar.gz
ln -s apache-maven-3.5.2 maven

echo -e '\nexport JAVA_HOME=/usr/local/jdk' >> $HOME/.bashrc
echo -e '\nPATH=$PATH:/usr/local/jdk/bin' >> $HOME/.bashrc
echo -e '\nPATH=$PATH:/usr/local/maven/bin' >> $HOME/.bashrc
