FROM andals/centos:7
MAINTAINER ligang <ligang1109@aliyun.com>

LABEL name="Jdk Image"
LABEL vendor="Andals"

COPY pkg/ /build/pkg/
COPY script/ /build/script/

RUN /build/script/build_image.sh

CMD /build/script/init.sh
