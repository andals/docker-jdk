# Init

```
git clone git@gitee.com:andals/docker-jdk.git  jdk
cd jdk
prjHome=`pwd`
jdkVer=8
```

# Build image

```
docker build -t andals/jdk:${jdkVer} ./
```
